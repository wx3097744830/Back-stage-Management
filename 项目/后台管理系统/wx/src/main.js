import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// 导入tree
import TreeTable from 'vue-table-with-tree-grid'
Vue.component('tree-table', TreeTable)

// 导入nprogress 
import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'

// 导入axios
import axios from 'axios'
// 配置请求的根路径
axios.defaults.baseURL = process.env.VUE_APP_API
// 在request请求拦截器中 展示进度条 Nprogress.start()
axios.interceptors.request.use(res => {
  Nprogress.start()
  res.headers.Authorization = window.sessionStorage.getItem("token")
  return res
})
// 在response响应拦截器中 隐藏进度条 Nprogress.done()
axios.interceptors.response.use(config=>{
  Nprogress.done()
  return config
})
Vue.prototype.$http = axios

// 导入element ui 全局样式
import ELEMENT from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ELEMENT);

// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(VueQuillEditor)

// 全局过滤时间
Vue.filter('dateFormat', function (time) {
  const currentTime = new Date(time)                                // 当前时间
  const y = currentTime.getFullYear()                               // 年
  const m = (currentTime.getMonth() + 1 + '').padStart(2, 0)        // 月 
  const d = (currentTime.getDate() + '').padStart(2, 0)             // 日
  const hh = (currentTime.getHours() + '').padStart(2, 0)           // 时
  const mm = (currentTime.getMinutes() + '').padStart(2, 0)         // 分
  const ss = (currentTime.getSeconds() + '').padStart(2, 0)         // 秒
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
