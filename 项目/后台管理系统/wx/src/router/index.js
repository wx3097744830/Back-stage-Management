import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: "/home"
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    redirect: "/Welcome",
    children: [
      {
        path: "/welcome",
        name: "Welcome",
        component: () => import("../views/Welcome.vue"),
      },
      {
        path: "/users",
        name: "Users",
        component: () => import("../views/users/Users.vue")
      },
      {
        path: "/rights",
        name: "Rights",
        component: () => import("../views/power/Rights.vue")
      },
      {
        path: "/roles",
        name: "Roles",
        component: () => import("../views/power/Roles.vue")
      },
      {
        path: "/categories",
        name: "Categories",
        component: () => import("../views/goods/Categories.vue")
      },
      {
        path: "/params",
        name: "Params",
        component: () => import("../views/goods/Params.vue")
      },
      {
        path: "/goods",
        name: "Goods",
        component: () => import("../views/goods/Goods.vue")
      },
      {
        path: "/AddGoods",
        name: "AddGoods",
        component: () => import("../views/goods/AddGoods.vue")
      },
      {
        path: "/orders",
        name: "Orders",
        component: () => import("../views/orders/Orders.vue")
      },
      {
        path: "/reports",
        name: "Reports",
        component: () => import("../views/reports/Reports.vue")
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let token = sessionStorage.getItem("token")
  if (token) {
    next()
  } else {
    if (to.path == "/login") {
      next()
    } else {
      next("/login")
    }
  }
})
export default router
